package util;

import java.util.ArrayList;
import java.util.List;

import persistence.NGram;

public class NGramGenerator {

    public NGramGenerator() {
        // TODO Auto-generated constructor stub
    }

    public static List<NGram> generateNGrams(List<String> word_list, int n) {
        List<NGram> result = new ArrayList<>();
        for (int i = 0; i < word_list.size() - 1; ++i) {
            List<String> words = new ArrayList<>();
            if ((i + n) <= word_list.size()) {
                for (int x = 0; x < n; x++) {
                    words.add(word_list.get(i + x));
                }
                
                NGram nGram = new NGram(words, 0);
                result.add(nGram);
            }
        }
        return result;
    }
}
