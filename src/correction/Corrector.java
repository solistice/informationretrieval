/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package correction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import persistence.NGram;
import persistence.NGramDB;
import persistence.Vocabulary;
import util.NGramGenerator;

/**
 *
 * @author ianvermeulen
 */
public class Corrector {

    private Vocabulary voc;
    private NGramDB bg;

    public Corrector(String vocFile, String bgFile) throws IOException {
        voc = new Vocabulary(vocFile);
        bg = new NGramDB(bgFile);
    }

    public static List<String> tokenizeSentence(String sentence) {
        // word regex
        Pattern p = Pattern.compile("\\w+");
        Matcher m = p.matcher(sentence.toLowerCase());

        // list for regex
        List<String> sentenceList = new ArrayList<>();
        while (m.find()) {
            sentenceList.add(m.group());
        }

        return sentenceList;
    }

    private String correctWord(int wordIndex, List<String> sentenceList) {
        return correctWord(wordIndex, sentenceList, -1);
    }
    
    private String correctWord(int wordIndex, List<String> sentenceList, int n) {
        if (n == -1) {
            n = bg.getN();
        }
        
        // get current word
        String word = sentenceList.get(wordIndex);

        HashMap<String, Integer> maxMap = new HashMap<>();
        
        for (String suggestedWord : voc.getEditsLs(word, 1)) {
            // put the word in every position of an ngram
            for(int wordPosition = 0; wordPosition < n; wordPosition++) {
                // check if the ngram can be taken at this position
                if (wordIndex >= wordPosition && sentenceList.size() >= wordIndex + n - wordPosition) {
                    // get the ngram position
                    List<String> nGramWords = sentenceList.subList(wordIndex - wordPosition, wordIndex + n - wordPosition);
                    nGramWords.set(wordPosition, suggestedWord);
                    NGram formedNGram = new NGram(nGramWords, 0);

                    int nGramFrequency = bg.getNGramFrequency(formedNGram);
                    
                    maxMap.put(suggestedWord, maxMap.getOrDefault(suggestedWord, 0) + nGramFrequency);
              
                }
            }
            
        
        }
        
        int maxFrequency = -1;
        String maxWord = "";
        
        Iterator it = maxMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>)it.next();
            
            if (pair.getValue() > maxFrequency) {
                maxFrequency = pair.getValue();
                maxWord = pair.getKey();
            }
        }
        
        return maxWord;
    }
    
    public String correct(String faultySentence) {
        List<String> sentenceList = tokenizeSentence(faultySentence);
        List<String> resultList = new ArrayList<>();

        for (int i = 0; i < sentenceList.size(); ++i) {
            // get current word
            String word = sentenceList.get(i);

            if (! (voc.hasWord(word)) ){
                String maxWord = correctWord(i, sentenceList);
                
                if (!maxWord.equals("")) {
                    // push new word to list
                    resultList.add(maxWord);
                }
                else {
                    resultList.add(word);
                }
            } else {
                // just add the word
                resultList.add(word);
            }

        }

        return String.join(" ", resultList);
    }
    
    public String correctContext(String faultySentence, int minFrequency, int edits, double alpha, double beta, double gamma) {
        boolean output = false;
        
        List<String> sentenceList = tokenizeSentence(faultySentence);
        
        List<NGram> sentenceBigrams = NGramGenerator.generateNGrams(sentenceList, bg.getN());
        
        String goodSentence = faultySentence;
        
        for(NGram currentNGram : sentenceBigrams) {
            NGram newNGram = bg.getBetterNGram(currentNGram, edits, alpha, beta, gamma, NGramDB.LS);

            if(output) {
            System.out.println("A better ngram for " 
                    + currentNGram
                    + "(" + bg.getNGramFrequency(currentNGram) + ")"
                    + " is " 
                    + newNGram
                    + "(" + bg.getNGramFrequency(newNGram) + ")");
            }

            if (newNGram != null) {
                goodSentence = goodSentence
                        .replace(currentNGram.toString(), 
                            newNGram.toString());

            }
        }
     
        return goodSentence;
    }

}
