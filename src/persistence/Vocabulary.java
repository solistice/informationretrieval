//code from http://raelcunha.com/spell-correct/
package persistence;

import info.debatty.java.stringsimilarity.Levenshtein;
import info.debatty.java.stringsimilarity.NGram;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ianvermeulen
 */
public class Vocabulary {

    private HashMap<String, Integer> nWords;
    private final Levenshtein ls = new Levenshtein();
    private final NGram ng = new NGram();

    public Vocabulary(String file) throws IOException {
        nWords = new HashMap<>();
        BufferedReader in = new BufferedReader(new FileReader(file));
        Pattern p = Pattern.compile("\\w+");
        for (String temp = ""; temp != null; temp = in.readLine()) {
            Matcher m = p.matcher(temp.toLowerCase());
            while (m.find()) {
                nWords.put((temp = m.group()), nWords.containsKey(temp) ? nWords.get(temp) + 1 : 1);
            }
        }
        in.close();
    }

    public Boolean hasWord(String word) {
        return nWords.containsKey(word);
    }

    public Integer getFrequency(String word) {
        word = word.toLowerCase();
        if (nWords.containsKey(word)) {
            return nWords.get(word);
        }

        return 0;
    }

    public List<String> getEditsLs(String word, int maxDistance) {
        List<String> result = new ArrayList<>();

        for (String currentWord : nWords.keySet()) {
            if (ls.distance(currentWord, word) <= maxDistance) {
                result.add(currentWord);
            }
        }

        return result;
    }
    
    public TreeSet<String> getEditsNg(String word, double maxDistance) {
        TreeSet<String> result = new TreeSet<>();

        for (String currentWord : nWords.keySet()) {
            if (ng.distance(currentWord, word) <= maxDistance) {
                result.add(currentWord);
            }
        }

        return result;
    }

    public void print() {
        SortedSet<String> keys = new TreeSet<>(nWords.keySet());
        for (String s : keys) {
            System.out.println(s + " " + nWords.get(s));
        }
    }

    public static void main(String args[]) throws IOException {
        Vocabulary v;
        v = new Vocabulary("assets/big.txt");
        v.print();
    }
}
