package persistence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author ianvermeulen
 */
public class NGram {
    
    private List<String> words;
    private Integer frequency;
    private info.debatty.java.stringsimilarity.Levenshtein ls;
    private info.debatty.java.stringsimilarity.WeightedLevenshtein wLs;
    private info.debatty.java.stringsimilarity.NGram ng;
    
    public NGram() {
        ls = new info.debatty.java.stringsimilarity.Levenshtein();
        wLs = new info.debatty.java.stringsimilarity.WeightedLevenshtein(new AzertyCharSub());
        ng = new info.debatty.java.stringsimilarity.NGram();
    }
    
    public NGram(String... words) {
        this();
        this.words = new ArrayList<>();
        this.words.addAll(Arrays.asList(words));
    }
    
    public NGram(List<String> words, int frequency) {
        this();
        this.words = new ArrayList<>();
        this.words.addAll(words);
        this.frequency = frequency;
    }

    public String getWord(Integer index) {
        if (index > words.size() || index < 0)
            return null;
        
        return words.get(index);
    }
    
    public List<String> getWords() {
        return words;
    }
    
    public Integer getN() {
        return words.size();
    }
    
    public Integer getFrequency() {
        return this.frequency;
    }
    
    public double getLsDistanceTo(NGram otherNGram) {
        if (getN() != otherNGram.getN()) {
            return -1;
        }
        
        double totalDistance = 0;
        
        for(int i = 0; i < words.size(); i++) {
            String thisCurrentWord = getWord(i);
            String otherCurrentWord = otherNGram.getWord(i);
            
            totalDistance += ls.distance(thisCurrentWord, otherCurrentWord);
        }
        
        return totalDistance;
    }

    public double getWeightedLsDistanceTo(NGram otherNGram) {
        if (getN() != otherNGram.getN()) {
            return -1;
        }

        double totalDistance = 0;

        for(int i = 0; i < words.size(); i++) {
            String thisCurrentWord = getWord(i);
            String otherCurrentWord = otherNGram.getWord(i);

            totalDistance += wLs.distance(thisCurrentWord, otherCurrentWord);
        }

        return totalDistance;
    }
    
    public double getNgDistanceTo(NGram otherNGram) {
        if (getN() != otherNGram.getN()) {
            return -1;
        }
        
        double totalDistance = 0;
        
        for(int i = 0; i < words.size(); i++) {
            String thisCurrentWord = getWord(i);
            String otherCurrentWord = otherNGram.getWord(i);
            
            totalDistance += ng.distance(thisCurrentWord, otherCurrentWord);
        }
        
        return totalDistance;
    }
    
    @Override
    public String toString() {
        String nGramString = "";
        
        if (words.isEmpty())
            return "";
       
        nGramString += words.get(0);
        
    	for (int i = 1; i < words.size(); i++) {
            nGramString += " " + words.get(i);
        }
        
        return nGramString;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof NGram))
            return false;
        
        NGram bigramObj = (NGram)obj;
        
        if (getN() != bigramObj.getN()) {
            return false;
        }
        
        return words.equals(bigramObj.getWords());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        
        for(String word : words) {
            hash = 41 * hash + Objects.hashCode(word);
        }
        
        return hash;
    }

}

