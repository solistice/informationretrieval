package persistence;

import info.debatty.java.stringsimilarity.Levenshtein;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 *
 * @author ianvermeulen
 */
public class NGramDB {
    public final static int NG = 1;
    public final static int LS = 2;
    public final static int WLS = 3;

    private String src;
    
    private HashMap<String, List<NGram>> firstList;
    private HashMap<String, List<NGram>> secondList;
    private HashMap<String, List<NGram>> thirdList;
    
    private HashMap<NGram, Integer> NGrams;
    
    private int maxFrequency;
    
    private Levenshtein ls = new Levenshtein();
    private int n = -1;
    
    
    public NGramDB() {
        src = "";
        firstList = new HashMap<>();
        secondList = new HashMap<>();
        thirdList = new HashMap<>();
        NGrams = new HashMap<>();
        maxFrequency = -1;

    }
    
    public NGramDB(String src) {
        this.src = src;
        firstList = new HashMap<>();
        secondList = new HashMap<>();
        thirdList = new HashMap<>();
        NGrams = new HashMap<>();
        maxFrequency = -1;
        
        readFromFile(src);
    }
    
    public int getN() {
        return n;
    }
    
    public List<NGram> getNGramFromWord(String word, int index) {
        List<NGram> nGrams = new ArrayList<>();
        
        // add all bigrams where the first word matches
        if ((index == 0) && (firstList.containsKey(word))) return firstList.get(word);
        else if ((index == 1) && secondList.containsKey(word)) return secondList.get(word);
        else if ((index == 1) && thirdList.containsKey(word)) return thirdList.get(word);
        else return nGrams;
    }
    
    public int getNGramFrequency(NGram bigram) {
        if (NGrams.containsKey(bigram)) {
            return NGrams.get(bigram);
        }
        
        return 0;
    }
    
    public NGram getBetterNGram(NGram nGram, int edits, double alpha, double beta, double gamma, int distAlgo) {        
        double originalFrequency = getNGramFrequency(nGram) / (double)maxFrequency;
        double maxScore = originalFrequency * alpha;
        NGram maxNGram = nGram;
        
        for(int fixedIndex = 0; fixedIndex < nGram.getN(); fixedIndex++) {
            NGram maxFixedNGram = null;
            double maxFixedScore = -1;
            
            List<NGram> currentFixedNGrams = 
                    getNGramFromWord(nGram.getWord(fixedIndex), fixedIndex);
            
            for(NGram currentNGram : currentFixedNGrams) {
                double currentFrequency = NGrams.get(currentNGram)/(double)maxFrequency;
                double currentEdits;
                double currentScore;
                switch (distAlgo) {
                    case LS:
                        currentEdits = nGram.getLsDistanceTo(currentNGram);
                        currentScore = (currentFrequency) / (double)(Math.pow(currentEdits * beta, gamma));
                        break;
                    case WLS:
                        currentEdits = nGram.getWeightedLsDistanceTo(currentNGram);
                        currentScore = (currentFrequency) / (double)(alpha * Math.pow(currentEdits * beta, gamma));
                        break;
                    default:
                        currentEdits = nGram.getNgDistanceTo(currentNGram);
                        currentScore = (currentFrequency) / (double)(Math.pow((1/(currentEdits * beta)), gamma));
                        break;
                }
                
                if (currentScore > maxFixedScore 
                        && (nGram.getLsDistanceTo(currentNGram) <= edits
                        || edits == -1)) {
                    maxFixedScore = currentScore;
                    maxFixedNGram = currentNGram;
                }
            }
            
            if( maxFixedScore > maxScore ) {
                maxScore = maxFixedScore;
                maxNGram = maxFixedNGram;
            }
        }
        
        return maxNGram;
    }
    
    private void readFromFile(String fileName) {
        FileReader fileReader = null;
        
        // read from file
        try {
            fileReader = new FileReader(fileName);
        } catch (FileNotFoundException ex) {
            System.out.println(fileName + " not found!");
        }
        
        BufferedReader reader = new BufferedReader(fileReader);
        String currentLine = null;
        try {
            while ( (currentLine = reader.readLine()) != null ) {
                // split on tab
                String[] columns = currentLine.split("\\t");

                // check if there are enough columns
                if (columns.length < 1) {
                    System.out.println("Incorrect column format detected");
                    continue;
                }
                
                if (n == -1)
                    n = columns.length - 1;

                Integer frequency = Integer.parseInt(columns[0]);
                if (frequency > maxFrequency) {
                    maxFrequency = frequency;
                }
                
                List<String> words = new ArrayList<>();
                for(int i = 1; i < columns.length; i++) {
                    String w = columns[i];
                    words.add(w);
                }
                NGram e = new NGram(words, frequency);
                NGrams.put(e, frequency);
                if (words.size() < 2) return;
                if (firstList.containsKey(words.get(0))) {
                    List<NGram> tmp = firstList.get(words.get(0));
                    tmp.add(e);
                    firstList.put(words.get(0), tmp);
                }
                else {
                    List<NGram> tmp = new ArrayList<>();
                    tmp.add(e);
                    firstList.put(words.get(0), tmp);
                }
                if (secondList.containsKey(words.get(1))) {
                    List<NGram> tmp = secondList.get(words.get(1));
                    tmp.add(e);
                    firstList.put(words.get(1), tmp);
                }
                else {
                    List<NGram> tmp = new ArrayList<>();
                    tmp.add(e);
                    secondList.put(words.get(1), tmp);
                }
                if (words.size() == 3) {
                    if (thirdList.containsKey(words.get(2))) {
                        List<NGram> tmp = thirdList.get(words.get(2));
                        tmp.add(e);
                        thirdList.put(words.get(2), tmp);
                    }
                    else {
                        List<NGram> tmp = new ArrayList<>();
                        tmp.add(e);
                        thirdList.put(words.get(2), tmp);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
}
