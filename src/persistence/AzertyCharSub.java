package persistence;

import info.debatty.java.stringsimilarity.CharacterSubstitutionInterface;


public class AzertyCharSub implements CharacterSubstitutionInterface {

    @Override
    public double cost(char c1, char c2) {

        // The cost for substituting vowels is lower
        if ((c1 == 'a' && c2 == 'e') || (c1 == 'e' && c2 == 'a')) {
            return 0.8;
        } else if ((c1 == 'a' && c2 == 'u') || (c1 == 'u' && c2 == 'a')) {
            return 0.8;
        } else if ((c1 == 'a' && c2 == 'i') || (c1 == 'i' && c2 == 'a')) {
            return 0.8;
        } else if ((c1 == 'a' && c2 == 'o') || (c1 == 'o' && c2 == 'a')) {
            return 0.8;
        } else if ((c1 == 'e' && c2 == 'u') || (c1 == 'u' && c2 == 'e')) {
            return 0.8;
        } else if ((c1 == 'e' && c2 == 'i') || (c1 == 'i' && c2 == 'e')) {
            return 0.8;
        } else if ((c1 == 'e' && c2 == 'o') || (c1 == 'o' && c2 == 'e')) {
            return 0.8;
        } else if ((c1 == 'u' && c2 == 'i') || (c1 == 'i' && c2 == 'u')) {
            return 0.8;
        } else if ((c1 == 'u' && c2 == 'o') || (c1 == 'o' && c2 == 'u')) {
            return 0.8;
        } else if ((c1 == 'i' && c2 == 'o') || (c1 == 'o' && c2 == 'i')) {
            return 0.8;
        } else if ((c1 == 'y' && c2 == 'i') || (c1 == 'i' && c2 == 'y')) {
            return 0.8;
        }
        // the cost for substituting letters which appear next to each other on the keyboard is lower
        else if ((c1 == 'a' && c2 == 'z') || (c1 == 'z' && c2 == 'a')) {
            return 0.9;
        } else if ((c1 == 'z' && c2 == 'e') || (c1 == 'e' && c2 == 'z')) {
            return 0.9;
        } else if ((c1 == 'e' && c2 == 'r') || (c1 == 'r' && c2 == 'e')) {
            return 0.9;
        } else if ((c1 == 'r' && c2 == 't') || (c1 == 't' && c2 == 'r')) {
            return 0.9;
        } else if ((c1 == 't' && c2 == 'y') || (c1 == 'y' && c2 == 't')) {
            return 0.9;
        } else if ((c1 == 'y' && c2 == 'u') || (c1 == 'u' && c2 == 'y')) {
            return 0.9;
        } else if ((c1 == 'o' && c2 == 'p') || (c1 == 'p' && c2 == 'o')) {
            return 0.9;
        } else if ((c1 == 'q' && c2 == 's') || (c1 == 's' && c2 == 'q')) {
            return 0.9;
        } else if ((c1 == 's' && c2 == 'd') || (c1 == 'd' && c2 == 's')) {
            return 0.9;
        } else if ((c1 == 'd' && c2 == 'f') || (c1 == 'f' && c2 == 'd')) {
            return 0.9;
        } else if ((c1 == 'f' && c2 == 'g') || (c1 == 'g' && c2 == 'f')) {
            return 0.9;
        } else if ((c1 == 'g' && c2 == 'h') || (c1 == 'h' && c2 == 'g')) {
            return 0.9;
        } else if ((c1 == 'h' && c2 == 'j') || (c1 == 'j' && c2 == 'h')) {
            return 0.9;
        } else if ((c1 == 'j' && c2 == 'k') || (c1 == 'k' && c2 == 'j')) {
            return 0.9;
        } else if ((c1 == 'k' && c2 == 'l') || (c1 == 'l' && c2 == 'k')) {
            return 0.9;
        } else if ((c1 == 'l' && c2 == 'm') || (c1 == 'm' && c2 == 'l')) {
            return 0.9;
        } else if ((c1 == 'w' && c2 == 'x') || (c1 == 'x' && c2 == 'w')) {
            return 0.9;
        } else if ((c1 == 'x' && c2 == 'c') || (c1 == 'c' && c2 == 'x')) {
            return 0.9;
        } else if ((c1 == 'c' && c2 == 'v') || (c1 == 'v' && c2 == 'c')) {
            return 0.9;
        } else if ((c1 == 'v' && c2 == 'b') || (c1 == 'b' && c2 == 'v')) {
            return 0.9;
        } else if ((c1 == 'b' && c2 == 'n') || (c1 == 'n' && c2 == 'b')) {
            return 0.9;
        }
        // For other cases, the cost of substituting 2 characters 1.0
        else {
            return 1.0;
        }
    }
}
