package spelling;

import benchmark.Training;
import benchmark.TrainingUnit;
import correction.Corrector;
import info.debatty.java.stringsimilarity.Levenshtein;
import java.io.IOException;
import java.util.Scanner;

public class Spelling {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Corrector corrector = null;
        boolean output = false;

        try {
            corrector = new Corrector("assets/english.50MB.txt", "assets/w3_.txt");
        } catch (IOException e) {
            System.out.println("Training files not found...");
            return;
        }
        
        System.out.println("Give a sentence: ");
        Scanner inputScanner = new Scanner(System.in);
        String input = inputScanner.nextLine();
        String correctedInput = corrector.correct(input);
        //String correctedInputContext = corrector.correctContext(correctedInput, 1, 3, 4, 5);
        
        System.out.println(correctedInput);

//        double alpha = 100.0;
//        double beta = 2.0;
//        double gamma = 6.0;
//
//        if (args.length == 3) {
//            alpha = Double.parseDouble(args[0]);
//            beta = Double.parseDouble(args[1]);
//            gamma = Double.parseDouble(args[2]);
//        }
//        
//        Training training = new Training("assets/training_set.txt");
//
//      
//        System.out.println("Performing corrections with [alpha = " + String.valueOf(alpha)
//                + ", beta = " + String.valueOf(beta)
//                + ", gamma = " + String.valueOf(gamma)
//                + "]");
//        
//        int correct = 0;
//        int total = 0;
//        
//        try {
//            TrainingUnit trainingUnit = null;
//
//            while ((trainingUnit = training.getNextTrainingUnit()) != null) {
//                total++;
//                String line = trainingUnit.getSentenceToCorrect();
//                String correctedLine = corrector.correct(line);
//                correctedLine = corrector.correctContext(correctedLine, 1, 2, alpha, beta, gamma);
//
//                if (trainingUnit.evaluateCorrection(correctedLine)) {
//                    correct++;
//                }
//                
//                if(output) {
//                    System.out.println("Corrected");
//                    System.out.println(line);
//                    System.out.println("To");
//                    System.out.println(correctedLine);
//                    System.out.println("--------");
//                    System.out.println("Solution was: " + trainingUnit.getCorrectionSolution());
//                    System.out.println("");
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Exception!");
//        }
//        
//        System.out.println("Finished correcting sentences:");
//        System.out.println("Score: " + correct + "/" + total);
//        System.out.println("Ratio: " + (double)correct/total);
        
    }
}
