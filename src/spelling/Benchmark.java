package spelling;

import benchmark.Training;
import benchmark.TrainingUnit;
import correction.Corrector;
import java.io.IOException;

public class Benchmark {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Corrector corrector;

        try {
            corrector = new Corrector("assets/english.50MB.txt", "assets/w3_.txt");
        } catch (IOException e) {
            System.out.println("Training files not found...");
            return;
        }

        double beta = 1.05;

        System.out.println("edits\talpha\tbeta\tgamma\tscore");

        for (int edits = 1; edits < 5; edits++) {
            for (double alpha = 10; alpha <= 50; alpha += 10) {
                for (double gamma = 1.0; gamma <= 8; gamma += 0.5) {
                    Training training = new Training("assets/training_set.txt");

                    int correct = 0;

                    try {
                        TrainingUnit trainingUnit = null;

                        while ((trainingUnit = training.getNextTrainingUnit()) != null) {
                            String line = trainingUnit.getSentenceToCorrect();
                            String correctedLine = corrector.correct(line);
                            correctedLine = corrector.correctContext(correctedLine, 1, edits, alpha, beta, gamma);

                            if (trainingUnit.evaluateCorrection(correctedLine)) {
                                correct++;
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Exception!");
                    }

                    System.out.println(edits + "\t"
                            + String.valueOf(alpha) + "\t"
                            + String.valueOf(beta) + "\t"
                            + String.valueOf(gamma) + "\t"
                            + correct + "\t");

                }
            }

        }
    }
}
