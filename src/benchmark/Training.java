/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author ianvermeulen
 */
public class Training {

    private BufferedReader trainingReader;
    
    public Training(String fileName) {
        try {
            FileInputStream in = new FileInputStream(fileName);
            trainingReader = new BufferedReader(new InputStreamReader(in));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public TrainingUnit getNextTrainingUnit() throws IOException {
        // skip
        trainingReader.readLine();
        // skip
        trainingReader.readLine();
        
        // wrong word
        String faultyWord = trainingReader.readLine();
        // correct word
        String correctWord = trainingReader.readLine();
        
        // skip
        trainingReader.readLine();
        
        // faulty sentence
        String faultySentence = trainingReader.readLine();
        
        // skip
        trainingReader.readLine();
        
        if (faultyWord == null 
                || correctWord == null 
                || faultySentence == null) {
            return null;
        }
        
        return new TrainingUnit(faultySentence, faultyWord, correctWord);
    }
}
