/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benchmark;

import correction.Corrector;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ianvermeulen
 */
public class TrainingUnit {
    private String faultySentence;
    private String faultyWord;
    private String correctedWord;
    
    public TrainingUnit(String faultySentence, String faultyWord, String correctedWord) {
        this.faultySentence = faultySentence;
        this.faultyWord = faultyWord;
        this.correctedWord = correctedWord;
    }
    
    public String getSentenceToCorrect() {
        return faultySentence;
    }
    
    public String getCorrectionSolution() {
        return faultyWord + " -> " + correctedWord;
    }
    
    public boolean evaluateCorrection(String sentence) {
        List<String> faultyTokens = Corrector.tokenizeSentence(faultySentence);
        List<String> tokens = Corrector.tokenizeSentence(sentence);
        
         if (faultyTokens.size() != tokens.size())
            return false;
         
        Integer faultyWordIndex = faultyTokens.indexOf(faultyWord);

        for (int i = 0; i < tokens.size(); i++) {
            if (i != faultyWordIndex) {
                String original = faultyTokens.get(i);
                String corrected = tokens.get(i);
                
                if (!corrected.equalsIgnoreCase(original))
                    return false;
            }
            else {
                String corrected = tokens.get(i);
                
                if (!corrected.equalsIgnoreCase(correctedWord))
                    return false;
            }
        }
        
        return true;   
    }
}
