#!/bin/bash
for alpha in 1 1.5 2 2.5 3 3.5 4 4.5 5
do
   for gamma in 1 1.5 2 2.5 3 3.5 4.5 5.5 6 6.5 7 7.5 8 8.5 9 9.5 10
   do
    java -jar dist/Spelling.jar alpha 1.05 gamma
   done
done